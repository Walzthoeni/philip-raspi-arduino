# Ansteuerung von Sensoren

Es gibt recht viele Sensoren für die unterschiedlichsten Einsatzzwecke.
Welche du schlussendlich einsetzen willst bleibt dir überlassen :) 
Um die einzelnen Sensoren anzusprechen werden die IO Pins des Arduino verwendet.
Dazu kann man in der Regel die Sensoren mit 3 Pins ansteuern.
Typischerweise gibt es 2 Pins für die entsprechende Stromversorgung und einen analogen Ausgang, welchen wir auslesen wollen.
Komplexere Sensoren liefern in der Regel auch gleich eine Library mit, welche nachinstalliert werden kann und dann erweiterte Funktionen bereitsstellt.

### Anmerkung

Meist werden Arduino Controller verwendet um Sensoren auszulesen, da diese im Gegensatz zum RasPi analoge Input/Output Pins haben.
Man kann dennoch einen RasPi dazu verwenden, jedoch wird erst ein A/D-Wandler benötigt, um das analoge Signal für die digitalen IO Pins des RasPi auszuarbeiten.

## Einfaches Beispiel

Hier wollen wir einfach einen Temperatursensor an einen ESP82466 hängen.
Die Temperatur sollte dann einfach in gewissen Zeitschriten ausgelesen werden und per serieller Schnittstelle ausgegeben werden.
Dazu erst die Verkabelung und dann die Programmstruktur :)

### Schaltplan

Der Schaltplan hier ist recht einfach: 
wir verwenden den 3.3V Output und GND um dem Sensor mit Strom zu versorgen und einen sogenannten Spannungsteiler um die Ausgangsspannung des Sensors von 3,3V auf 0-1V zu reduzieren. 
Dies wird benötigt, da der ADC Eingang nur bis 1V messen kann (ansonsten misst er nur den Wert 1024).

![](images/Schaltplan.jpg)

### Aufbau

Erst wieder einfach testen mit einem Breadboard.

![](images/Aufbau.jpg)

### Code

Das Projekt hab ich mit PlatformIO aufgebaut.
Dieses Framework erleichtert einiges bei der Entwicklung an Arduinoboards und ist in Kombination mit VS Code eine spitzen Platform.
Dieses Framework lädt dir für alle vorhandenen Controller die entsprechenden Libraries runter und kann auch andere third-party Libraries runterladen und sehr einfach einbinden (dies ist mit dem ArduinoStudio teilweise ein totaler Krampf!)

Die entsprechenden Codezeilen findest du unter "source/Temperatursensor/src/main.cpp"

Falls du lieber das ArduinoStudio verwendest, ist dies auch kein Problem.
Einfach die main.cpp in \<Projektname\>.ino umbenennen oder den Code in bestehendes Projekt kopieren :)

### Infos für den verwendeten Arduino in diesem Beispiel

Hier noch ein paar Datenblätter für den Sparkfun ESP2866 Thing Dev Controller:

* https://github.com/esp8266/Arduino
* im Folder "doc" findest du ein pdf mit dem Schematischen Aufbau des Controllers
* https://learn.sparkfun.com/tutorials/esp8266-thing-development-board-hookup-guide