#include <Arduino.h>

const int analogInPin = A0;  // ESP8266 Analog Pin ADC0 = A0

int sensorValue = 0;  // value read from the sensor

void setup() {
  // initialize serial communication at 115200 baud rate
  // if changed, it has to be altered in platformio.ini under monitor_speed
  Serial.begin(115200);
}

void loop() {
  // read the analog in value
  sensorValue = analogRead(analogInPin);
 
  // print the read value in the Serial Monitor
  Serial.print("temperture = ");
  Serial.print(sensorValue);
  Serial.print("\n");
  
  // wait a second
  delay(1000);
}