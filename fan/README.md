# Ventilatorsteuerung

Diese Steuerung kann behilflich sein, da die Entwickler des RaspberryPi 4 einen Fahler gemacht haben.
Sobald sich der Rechner in einem Case befindet kann es leicht passieren, dass unter Last die Temperatur der CPU zu weit nach oben geht.
Dafuer habe ich eine Case gekauft, welches bereits einen Ventilator besitzt (notfalls funktioniert der RasPi auch, wenn er ohne Case betrieben wird).

Um diesen jedoch korrekt einzubinden, muessen einige Dinge erledigt werden!
Diese (recht kleinen Arbeiten) helfen dir aber schon grundsaetzlich eine Betriebssystem besser zu verstehen, wie man mit den GPIO Pins umgeht, wie man mit Elektronik Dinge steuern kann.

## Elektrische Steuerung

Hier gibt es leider schon das erste Problem.
Und zwar hat der RasPi nur einen unschaltbaren 5V Output welcher fuer den Ventilator benoetigt wird.
Der Ventilator kann natuerlich direkt angeschlossen werden, jedoch produziert dieser zuviele Geraeusche, die wir einfach nicht haben wollen.
Um die 5V zu schalten verwenden wir einen der steuerbaren GPIO Pins (3.3V) des RasPi, um mittels eines Transistors die 5V fuer den Ventilator freizugeben.
Der Schaltungsaufbau findest du im Bild "Schaltplan.jpg".

![](images/Schaltplan.jpg)

### Kurzerklaerung des Schaltplan

Auf der linken Seite findest du die GPIO Pins den RasPi.
Eine komplette Auflistung aller Pins, ihrer Funktionen und internen Bezeichnungen findest du im File GPIO-Pinout.png

![](images/GPIO-Pinout.png)

Von hier werden 3 Pins benoetigt: den Pin1 (5V Output), Pin 6 (GND oder Ground/Masse) und Pin 12 (welcher intern mit der GPIO Nummer 18 angesprochen wird).
Der Pin fuer GND wir mit dem Collector des Transistors verbunden (die Begriffserklaerungen findest du [hier](https://www.build-electronic-circuits.com/how-transistors-work/)).
Die sogenannte Basis wird mit einem Vorwiderstand vom 650 Ohm mit dem Pin 12 (oder GPIO18) verbunden.
Dieser Ausgang steuert dann das Verhalten des Transistors.
Schlussendlich wird der Pin 1 mit seinen 5V direkt mit dem roten Stecker des Ventilators verbunden und der Emitter des Transistors mit dem schwarzen Kabel.
Zusaetzlich wird eine Freilaufdiode eingebaut um bei der Abschaltung des Motors auftretende Spannungsspitzen abzufangen und den RasPi davor zu schuetzen.

Mit dem Breadboard schaut die ganze Schaltung dann auf wie folgt:

![](images/Schaltungsaufbau.jpg)

## Programm zur Steuerung

Da ich mir nicht ganz sicher bin, ob ihr die Skripsprache Python schon behandelt habt, habe ich dir das Programm schon vorgefertigt.
Der Aufbau hier ist relativ einfach und aehnlich den C-Codes die du schon kennst.
Ein paar Ausnahmen oder Neuerungen gibt es aber trotzdem:

* in der ersten Zeile steht folgendes "#!/usr/bin/env python3". 
Dies sagt dem Betriebssystem mit welche Programm das Skript aufgerufen weden soll.
Hier ist es der Python3 Interpreter

* def foo():
Dies ist eine Funktionsdefinitio aehnlich wie in C (void foo(){})

* die letzten beiden Zeilen des Progamms machen einen einfach Trick: 
diese Zeile wird nur dann ausgefuehhrt, wenn diesen File direkt aufgerufen wird (ansonsten hat __name__ einen anderen Wert).
Da man in Python recht einfach auch andere Pythonfiles inkludieren kann und deren Funktionen verwenden kann wird so gewaehrleistet, dass nur die main()-Funktion dieses Files aufgerufen wird.

Fuer alle anderen Zeilen des Codes habe ich dir Kommentare hinterlassen :)

Was noch wichtig ist, ist dass das Skript die Rechte hat, ass es auch als Programm ausgefuehrt werden kann.
Dafuer einfach entweder im Filebrowser mit der rechten Maustaste drauchklicken, Eigenschaften/Properties auswaehlen und im Reiter Rechte/Permissions das Ausfuehren/Execute auf Jeder/Anyone stellen.

Wenn du nun auf der Kommandoyeile "./fan.py" ausfuehrst, sollte das Programm starten :)

## Einbindung des Programms in das Betriebssystem

Nun muss das Programm, welches uns die Ventilatorsteuerung uebernimmt noch so eingepflegt werden, dass das Skript mit Start des RasPi auch startet.
Das geht recht einfach, kann aber mit mehreren verschiedenen Varianten gemachten werden (machen aber schlussendlich das gleiche ! WICHTIG ! nur eine der Varianten machen ;) ):

* die einfachste Moeglichkeit ist, das Skript in die Datei /etc/rc.local einzufuegen. 
Einfach in die letzte Zeile VOR "exit 0" ./path/to/file einfuegen und das Skript startet automatisch

* eleganter und zeitgemaesser ist die Verwendung sogenannten cronjobs.
Diese koennen verwendet werden, um verschiedene Programme zu bestimmten Zeitpunkten zu starten (egal ob "jede Woche Sonntags um 00:00" oder "bei Neustart").
Dafuer auf der Kommandoyeile "crontab -e" aufrufen, dann einen Editor auswaehlen (nimm nano, der ist am einfachsten zu bedienen (speichern mit strg+o, schliessen mit strg+x)) und folgende Zeile reinschreiben: @reboot /path/tp/file

