#!/usr/bin/env python3

import os
import signal
import sys
import RPi.GPIO as GPIO
import time

pin = 18

def main():
    
    # some general setup things
    GPIO.setmode(GPIO.BCM)
    
    # set IO Pin to output mode
    GPIO.setup(pin, GPIO.OUT)
    
    # disable warnings
    GPIO.setwarnings(False)

    while(True):
    
        # check CPU temperature
        cpu_temperature = get_cpu_temp()
        
        # decide if fan should be switched on or not
        if cpu_temperature > 70:
            GPIO.output(pin, True)
        else:
            GPIO.output(pin, False)
            
        # sleep a few seconds to give back computing power to kernel.
        # This step is important, as otherwise the RasPi would only
        # check for temperatures and nothing else!
        time.sleep(5)

def get_cpu_temp():
    
    # Initialize the result.
    result = 0.0
    
    # The first line in this file holds the CPU temperature as an integer times 1000.
    # Read the first line and remove the newline character at the end of the string.
    with open('/sys/class/thermal/thermal_zone0/temp') as f:
        line = f.readline().strip()
    
    # Test if the string is an integer as expected.
    if line.isdigit():
        # Convert the string with the CPU temperature to a float in degrees Celsius.
        result = float(line) / 1000
    
    # Give the result back to the caller.
    return result

if __name__ == "__main__":
    main()
